Hardware
------------

The vislab consists of four workstations, ``monet``, ``picasso``, ``dali``, and ``warhol``.
Three of these (``picasso``, ``dali``, and ``warhol``) are set up at desks.
The fourth system (``monet``) drives the display wall.
The display wall is a 10 foot wide display covers one wall and consists of 3x3 grid of displays with an aggregate resolution of 5780x3260 pixels.


Each contains:

* Two Intel Xeon E5-2680 CPUs (20 CPU cores total)
* 64 GB DRAM
* 128 GB SSD for the OS
* 3 TB HD for scratch space
* 10Gb Ethernet
* Quadro K6000 GPU

``picasso``, ``dali``, and ``warhol`` are each connected to a 32 inch 4k displays (ASUS PQ321 resolution: 3840x2160).
``monet`` contains three Quadro K6000's to drive the display wall.
The display wall appears to the OS as a single large display, so no special software is required to use it.
Any application that runs on the system can be scaled to the entire wall.

The powerful CPUs, GPUs, and large RAM capacity allow these systems to load and analyze large datasets interactively.
The 10Gb Ethernet allows fast access to University NFS storage, Flux, or remote sites like XSEDE and Blue Waters.
