Frequently asked questions
==========================

1. How do I display PDF files?

Use ``evince``, it is the most stable PDF reader on the high resolution displays in the lab.
On the start menu, this shows up as *Document Viewer*
On the command line, you can ``open file.pdf`` or ``evince file.pdf``.
