Access
-------------

The vislab is located in Biointerfaces Computational Assembly Lab, NCRC B10-G034.
It is behind a door that is usually unlocked during normal business hours.
Accounts on the systems are created on request to students, post docs, faculty, and staff in the Biointerfaces Institute.
Fill out this form: `<https://goo.gl/i8Cw9p>`_ to request an account.
You will be notified by e-mail when your account is ready.

Once you have an account, you can login to any of the machines with your University of Michigan *uniqname* and *kerberos password*.
Log out when you are done using the system, and do not leave jobs running in the background.

.. figure:: layout.*
    :width: 70%
    :align: center

    Vislab layout

.. figure:: map.*
    :width: 70%
    :align: center

    To find NCRC B10-G034, enter the NCRC via the building 18 main entrance and follow this map to G034.
