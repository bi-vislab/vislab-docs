Data storage
------------

All four vislab workstations have:

* Shared NFS directories: ``/nfs/vizlabhome/$USER``
* A 3TB HD for local scratch space: ``/scratch/$USER``
* 10Gb Ethernet for fast access to NFS mounted data ``/nfs/$GROUP``

Home directories
~~~~~~~~~~~~~~~~

The desktop workstations use ``/nfs/vizlabhome/$USER`` as your home directory so that you can easily sit at any of the three workstations and resume working with the same data and environment.
Each of these systems has a separate scratch drive.

On the display wall system, your home directory is on the local scratch drive.
It is separate from the NFS mounted home directory because you may want to set window manager settings different to make the display wall more usable.
You can access data in your NFS home directory from ``/nfs/vizlabhome/$USER`` on the display wall system.

Space availability
~~~~~~~~~~~~~~~~~~

Each user can store up to 8 GB in their NFS home directory ``/nfs/vizlabhome/$USER``.
Files stored in the home directory are permanent and persist indefinitely.
Use your home directory to store configuration files, code, and software.

Scratch space in ``/scratch/$USER`` is meant for temporary storage of project data.
It is local to each workstation, so if you copy data to ``/scratch`` on one system, it is not available on the others.
Use scratch space to temporarily store large datasets on the workstation for fast local access, visualization, or analysis.
There is no quota for usage on ``/scratch``, but files older than 90 days may be purged without warning to ensure there is enough space for other users.

NFS mounted storage is available in ``/nfs/$GROUP``.
Biointerfaces does not provide NFS storage to groups.
If you group has their own storage on the campus Value Storage or High Performance Storage systems, the vislab admins can mount it on request (see :any:`support`).

Backups
~~~~~~~

``/scratch`` is **NOT BACKED UP**.
The scratch drives are redundant RAID1 to prevent data loss due to drive failure, but there are no backups to recover deleted or previous versions of files.

``/nfs/vizlabhome`` has limited snapshot backups going back a few weeks.
If you accidentally delete a file, or want to access a previous version, you can do so yourself.
In a terminal, execute::

    $ ls /nfs/vizlabhome/.snapshot

You will see a list of time codes, such as ``daily.2015-05-10_0010``.
Choose the time code you want ``$TIMECODE``.
By changing directories, you can access any files in your home directory as they were at that time::

    $ cd /nfs/vizlabhome/.snapshot/$TIMECODE/$USER

Snapshots are available for up to three weeks in the past.
File states older than that are no longer accessible.
