Software
-----------------------

Standard linux software
~~~~~~~~~~~~~~~~~~~~~~~

Standard linux software is available locally on each of the workstations.
You can launch it either from the start menu in the lower left corner of the screen or via the terminal.
If there are standard linux packages that you would like installed, submit a request (see :any:`support`).

Advanced visualization tools
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are currently no specialized visualization tools installed on the vislab machines. You are free to install
such software in your home directory.
Submit a request (see :any:`support`), if you would like one installed on all the machines.

Group specific software
~~~~~~~~~~~~~~~~~~~~~~~

If your group has standard software that they would like to install, contact the administrators (see :any:`support`)

Glotzer group software is available on the vislab machines. It is already installed on the system and
requires no additional actions (such as module load commands) to access.
