Terms of usage
===========================================================


#.  **Scratch Data is not backed up.** Scratch data on the vislab is not backed up. The data that you keep in your home directory, /tmp or any other filesystem is exposed to immediate and permanent loss at all times. You are responsible for mitigating your own risk.
#. **Your usage is tracked and may be used for reports and security purposes**.
#. **The vislab is for interactive use**. These systems are configured and purchased specifically for interactive use at the workstation. We do not allow remote access or background jobs.
#. **You are responsible for the security of sensitive codes and data.** If you will be storing export-controlled or other sensitive or secure software, libraries, or data on the vislab workstations, it is your responsibility that is is secured to the standards set by the most restrictive governing rules.  We cannot reasonably monitor everything that is installed on the systems, and cannot be responsible for it, leaving the responsibility with you, the end user.
#. **Data subject to HIPAA regulations may not be stored or processed on the vislab workstations.**

Access:

* The lab is accessible to students, post docs, faculty, and staff in the Biointerfaces Institute.
* Login accounts are created on request.
* Keys to B10-G034 will not be issued to vislab users, unless their assigned desk is in the G034 area.
* The door to B10-G034 is usually unlocked during normal business hours.
* Systems are for interactive use only. Users may be auto-logged out after 45 minutes of inactivity, and background processes killed.
* Log in to only one system at a time to leave the rest available for other users.

Data:

* Users are provided a home directory on NFS, common to ``picasso``, ``dali``, and ``warhol`` with 8 GB of space.
* Users are free to use as much space in ``/scratch/$USER`` as they need.
* Files in ``/scratch`` older than 90 days may be purged without notice

The Biointerfaces visualization lab administrators will:

* Keep the workstations in working order.
* Apply security patches.
* Perform software updates.
* Create login accounts for users.
* Install standard linux software tools on request.
* Install standard visualization tools on request.
* Troubleshoot and resolve hardware problems.
* Troubleshoot and resolve problems with software installed by the administrators.
* Add Value Storage and High Performance Storage NFS mounts on request.

Users are responsible for:

* Using the systems responsibly.
* Following all policies outlined in this document.
* Installing highly specialized software in their home directory.
* Reporting problems to the administrators.

Users that willfully violate vislab polices will have their accounts locked.
