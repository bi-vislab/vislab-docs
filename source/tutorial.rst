Tutorial
------------------

Powering systems on
~~~~~~~~~~~~~~~~~~~

All workstations should remain on at all times.
If you find that a system is off, you can press the power button to turn it on.
``dali`` is located underneath the desk on the east side of the room.
``monet``, ``picasso``, and ``warhol`` are in the rack in the southwest corner.
To turn on one of these systems, carefully open the front glass door, reach in and press the power button on the appropriately labeled system.

Displays
~~~~~~~~~~~~~~~~~~~

On the desktop workstations, press any key to wake the display up from sleep.
Each system has a keyboard and mouse on the desk in front of the display.
If the display is not on, press the power button on the right hand side.
These displays do have built-in speakers.
Use the volume control on the right hand side to set an appropriate volume level.

The display wall is driven by the wireless keyboard and mouse.
It does not automatically turn on.
To turn on the display wall, press the **volume up** key in the upper right part of the wireless keyboard.
Usually, the wireless keyboard and mouse are on the desk in the middle of the room that faces the display wall.
You may find the wireless keyboard plugged in to charge on top of the rack in the southwest corner of the room.

The displays **ARE NOT** touch screens.
Please do not touch them.

Logging in
~~~~~~~~~~~~~~~~~~~

After waking a display from sleep, you should find the system at a login screen.
To login in, type in your University of Michigan *uniqname* in the user field, then your *kerberos password* in the password field, and choose a **Session** type.
Press enter to login.

If you do not have an account, request one before you can login: see :any:`access`.
If you know you have an account and still cannot login, report the problem to the system administrators (see :any:`support`).

The following sessions are available:

* `KDE Plasma Desktop <https://www.kde.org/>`_ - **recommended**
* `XFCE <http://xfce.org/>`_
* `MATE <http://mate-desktop.org/>`_

KDE Plasma offers reasonable default font scaling on the high density displays. XFCE is a more minimal environment.
MATE often has trouble displaying toolbars properly at high resolution.

Logging out (Plasma)
~~~~~~~~~~~~~~~~~~~~

To log out, click the start menu (the *K*) at the lower left hand side of the screen.
Select the log out option on the menu.
On the desktop workstations, the display will automatically go to sleep after a period of inactivity.

If you used the display wall, please turn off the displays when you are done.
To turn off the displays, press the **mute** button on the wireless keyboard.
If you notice the wireless keyboard showing a low battery state, please set it on top of the rack in the southwest corner and plug it into the USB cable to charge.
The keyboard's battery is weak and only lasts for approximately one week of use between recharges.

If you are using a session other than Plasma, refer to your desktop environment's documentation for logout instructions.

Launching software (Plasma)
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Most standard linux software can be launched from the start menu (the *K*) at the lower left hand side of the screen.
Software is categorized into groups, or you can use the search box to find what you are looking for.
Alternately, you can use the text based application launcher.
To trigger this launcher, pres ``<Alt>+<F2>``, type the name of the program you wan to run, select it from the drop down with the arrow keys and then press ``<Enter>``.
For example: ``<Alt>+<F2>term<down><Enter>`` launches the terminal.
If there is a standard linux software package that you would like installed, contact the system administrators and request it (see :any:`support`).

If you are using a session other than Plasma, refer to your desktop environment's documentation instructions on how to launch software.
