Acknowledging use
=================

Please acknowledge the use of the Biointerfaces Visulization Lab:

* When you use the vislab systems to generate figures for a publication or supplementary information
* When the systems enabled scientific insights

This work used the Biointerfaces Institute Visualization Lab at the University of Michigan.
