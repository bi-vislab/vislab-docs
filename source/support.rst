Technical support
======================

The vislab does not have dedicated support staff.
It is supported part time by graduate students in Sharon Glotzer's research group.
To make an official support request to the system administrators, send an e-mail to bi-vislab@umich.edu.

Typical support requests include:

* Report software or hardware problem
* Problem logging in
* Request software installation
* Request an NFS mount

The vislab administrators do not have time or resources to train users in the use of specialized visualization software or on visualization techniques.
Users will need to learn from other graduate students, or use the highly recommended training and consulting services that the UM3D Lab (`<http://um3d.dc.umich.edu/>`_) provides **for free**.
