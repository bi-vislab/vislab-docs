Biointerfaces Visualization Lab
===============================

The Biointerfaces Visualization Lab (vislab) is a computer lab with 4 powerful graphics workstations available for use by the `Biointerfaces Institute <http://biointerfaces.umich.edu/>`_ at the University of Michigan.
It's purpose is to enable researchers to interactively visualize high-resolution, large scale simulation output or experimental data.
Example uses include:

* Visualizing simulation / experimental data
* Interactive data analysis and exploration
* Visualization of real-time simulation results
* Small group discussions around the 10-ft visualization wall

Funding for the vislab is provided by the Biointerfaces Institute and UMOR.
It is designed in cooperation with a larger campus wide effort by the UM3D lab to provide visualization hubs throughout campus.

See the latest version of this documentation online: http://biointerfaces-visualization-lab.readthedocs.org/

Contents:

.. toctree::
    :maxdepth: 2

    access
    hardware
    tutorial
    software
    data
    faq
    support
    acknowledge
    service
